package com.j2eefast.framework.job.task;

import cn.hutool.json.JSONUtil;
import com.j2eefast.framework.quartz.entity.SysJobEntity;
import com.j2eefast.framework.quartz.utils.AbstratBusJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 关联业务定时任务样例
 * 1. 继承 AbstratBusJob
 * 2. 重写 execute 方法
 * 3. 写业务逻辑
 * 4. sysJob 类有你的业务主键ID
 */
@Component("busBizTask")
@Slf4j
public class BusBizTask extends AbstratBusJob {



    @Override
    protected void execute(SysJobEntity sysJob) {
        log.info("定时任务开始执行了 -------------");
        log.info("定时任务对象数据:{}", JSONUtil.toJsonStr(sysJob));
    }
}
