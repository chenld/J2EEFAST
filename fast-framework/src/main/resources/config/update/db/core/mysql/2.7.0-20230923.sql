ALTER TABLE sys_config ADD COLUMN `app_no` varchar(50) NULL DEFAULT 'default' COMMENT '应用编码' AFTER `id`;
ALTER TABLE sys_config ADD COLUMN `app_name` varchar(255) NULL DEFAULT '本系统' COMMENT '应用名称' AFTER `app_no`;
ALTER TABLE sys_dict_type DROP INDEX `dict_type`,ADD INDEX `dict_type`(`dict_type`) USING BTREE;